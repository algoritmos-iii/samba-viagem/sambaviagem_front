SambaViagem
Projeto Front-end para a cadeira de Algoritmos III, Senac Pelotas. Utiliza a framework Vue.js e Bootstrap. O projeto Front tem o intuito de ser utilizado pelos funcionários da empresa, a qual poderão cadastrar e ler informações fornecidas pelo Back-End.

### Módulos Principais

- Vue.js
- Json
- Bootstrap
- MySQL

### Como rodar o front

1. Abra o Prompt de Comando
1. Vá até a pasta raiz do projeto
1. Será necessário rodar os comandos, localizados na aba abaixo 
1. utilize o comando "npm run serve" para executar o vue
1. Verifique a porta e cole no navegador "localhost:<porta>"
1. O projeto já estará aberto para teste

### Comandos necessários para rodar:

npm install --save chart.js hchs-vue-charts jspdf jspdf-autotable

#### Front no Heroku - https://sambaviagem-front.herokuapp.com/
#### API Spring Boot - Https://javatec2.herokuapp.com
#### Postman - https://bold-space-3147-1.postman.co/collections/6535963-b72e7672-b728-47f9-a61e-f9eb7431739a?version=latest&workspace=1dd0e5cb-5ff8-4cc5-8d8d-e47c0426cae4

Contribuição
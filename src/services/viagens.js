import { http } from './config'

export default {

    listar:() => {
        return http.get('viagens')
    },

    salvar:(viagem) => {
        console.log(viagem);
        return http.post('viagens', viagem)
    },

    atualizar:(viagem) => {
        return http.put('viagens', viagem)
    },

    apagar:(viagem) => {
        return http.delete('viagens', {data: viagem})
    },

    getV:(id) => {
        return http.get('viagens', id)
    }
}